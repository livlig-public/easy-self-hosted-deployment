#!/bin/bash
#get and run update.sh
wget https://gitlab.com/livlig-public/update_script/-/raw/main/update.sh /root/update.sh
chmod 777 /root/update.sh
./update.sh

#Basic utilities (desktops and servers)
sudo apt install -y vim bash-completion net-tools wget curl lsof htop locate zip unzip p7zip p7zip-full p7zip-rar

#Remove snapd
sudo systemctl stop snapd
sudo apt remove --purge --assume-yes snapd gnome-software-plugin-snap
sudo rm -rf ~/snap/ /var/cache/snapd/ 

#install cockpit
sudo apt install -y cockpit

#install docker repo and requirements https://docs.docker.com/engine/install/ubuntu/
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt install  -y ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo chmod a+r /etc/apt/keyrings/docker.gpg
sudo apt update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

#make directories
mkdir /data
mkdir /data/docker
mkdir /data/backups
mkdir /data/games
mkdir /data/downloads
mkdir /data/public
mkdir /data/media
mkdir /data/media/Movies
mkdir /data/media/TV
mkdir /data/media/Books
mkdir /data/media/Audiobooks
mkdir /data/media/Comics
mkdir /data/media/Music
mkdir /data/media/Photos
mkdir /data/media/Videos
