# Easy Self Hosted Deployment
## This is very early in development and a WIP!
**GOAL**: Automatically deploy selfhosted infrastructure and services with minimal manual configuration.

Currently testing on Ubuntu Server 22.04 LTS.

# TO DO
- user warnings
    - make sure server has static IP or DHCP reservation
    - 
- user input:
    - lets start with "Easy mode"
        - lookup ip, subnet, DGW, domain of host.
    - later can add manual config option
    - domain name

- Auto configuration of services:
  - pihole
  - proxy
  - passwords?

# What it does
## Dependencies 
- Cockpit
- Docker 
- update script
### Dirs
/data/docker 
/data/media/ etc

## Containers 
### Infra
#### Networking  
- DNS
- Proxy 
- DDNS Updater 
- Wireguard 
#### Container Management
- Yacht 
- Portainer 
- watchtower 
#### monitoring 
- uptime kuma
- speedtest tracker 
- Libre speed

### Media 
- Jellyfin 
- Jellyseer
- Kavita 
- Photo Prism

### DVR 
- qbittorrent 
- prowlarr 
- Radarr 
- Sonaar 
- Readarr 
- Lidarr 

### Other 
- Syncthing 
- FileBrowser 
- Heimdall

## Configuration
?

## Cleanup 
?

# Future ideas 
- 2nd DNS 
- Crowdsec 
- Configure OMV?