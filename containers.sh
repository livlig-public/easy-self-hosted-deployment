#!/bin/bash
#==========================================
#Infra
#==========================================

#Network
#------------------------------------------

#Nginx Proxy Manager
docker run -d \
  --name=nginx-proxy-manager \
  -p 80:80/tcp \
  -p 443:443/tcp \
  -p 81:81/tcp \
  -v /data/docker/nginx-proxy/data:/data \
  -v /data/docker/nginx-proxy/letsencrypt:/letsencrypt \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart unless-stopped \
  qmcgaw/ddns-updater

#Pi-Hole - DNS + Ad Blocker
docker run -d \
  --name=pihole \
  -p 53:53/tcp -p 53:53/udp \
  -v "/data/docker/pihole/etc/pihole:/etc/pihole" \
  -v "data/docker/pihole/etc/dnsmasq.d:/etc/dnsmasq.d" \
  --dns=127.0.0.1 --dns=149.112.121.20 --dns=149.112.122.20 \
  --hostname pi.hole \
  -e VIRTUAL_HOST="pi.hole" \
  -e PROXY_LOCATION="pi.hole" \
  -e FTLCONF_LOCAL_IPV4="127.0.0.1" \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  pihole/pihole:latest

#DDNS-Updater
docker run -d \
  --name=ddns-updater \
  -v /data/docker/ddns-updater:/updater/data \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \  qmcgaw/ddns-updater:latest

#Wireguard VPN
docker run -d \
  --name=wireguard \
  --cap-add=NET_ADMIN \
  --cap-add=SYS_MODULE \
  -p 51820:51820/udp \
  -v /data/docker/wireguard/config:/config \
  -v /lib/modules:/lib/modules \
  --sysctl="net.ipv4.conf.all.src_valid_mark=1" \
  -e SERVERURL=wireguard.domain.com `#optional` \
  -e SERVERPORT=51820 `#optional` \
  -e PEERS=1 `#optional` \
  -e PEERDNS=auto `#optional` \
  -e INTERNAL_SUBNET=10.13.13.0 `#optional` \
  -e ALLOWEDIPS=0.0.0.0/0 `#optional` \
  -e LOG_CONFS=true `#optional` \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/wireguard:latest

#Container Management
#------------------------------------------
#Yacht
docker run -d \
  --name=yacht \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /data/docker/yacht:/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  selfhostedpro/yacht:latest

#Portainer
docker run -d \
  --name=portainer \
  -p 8000:8000 \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /data/docker/portainer:/data \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  portainer/portainer-ce:latest

#Watchtower
docker run -d \
  --name watchtower \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  containrrr/watchtower:latest

#Monitoring
#------------------------------------------
# Uptime Kuma
docker run -d \
  --name uptime-kuma \
  -p 3001:3001 \
  -v /data/docker/uptime-kuma:/app/data \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  louislam/uptime-kuma:latest

# Speedtest Tracker
docker create \
  --name=speedtest-tracker \
  -v /data/docker/speedtest-tracking:/config \
  -e OOKLA_EULA_GDPR=true \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  henrywhitaker3/speedtest-tracker

#LibreSpeed
docker run -d \
  --name=librespeed \
  -e PASSWORD=PASSWORD \
  -e CUSTOM_RESULTS=false `#optional` \
  -e DB_TYPE=sqlite `#optional` \
  -e DB_NAME=DB_NAME `#optional` \
  -e DB_HOSTNAME=DB_HOSTNAME `#optional` \
  -e DB_USERNAME=DB_USERNAME `#optional` \
  -e DB_PASSWORD=DB_PASSWORD `#optional` \
  -e DB_PORT=DB_PORT `#optional` \
  -v /data/docker/librespeed:/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/librespeed:latest

#==========================================
#Media
#==========================================
#Jellyfin
docker run -d \
  --name=jellyfin \
  #-e JELLYFIN_PublishedServerUrl=192.168.0.5 `#optional` \
  -p 7359:7359/udp `#discovery` \
  -p 1900:1900/udp `#DNLA` \
  -v /data/docker/jellyfin:/config \
  -v /data/media/TV:/data/tvshows \
  -v /data/media/Movies:/data/movies \
  -v /data/media/Videos:/data/videos \
  -v /data/media/Music:/data/music \
  -v /data/media/Audiobooks:/data/audiobooks \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/jellyfin:latest

#Jellyseer
docker run -d \
  --name jellyseerr \
  -v /data/docker/jellyseer:/app/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  fallenbagel/jellyseerr:latest

#Kavita
docker run --name kavita -p 5000:5000 \
  -v /data/media/Books:/books \
  -v /data/docker/kavita:/kavita/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  -d kizaing/kavita:latest

#PhotoPrism
docker run -d \
  --name photoprism \
  --security-opt seccomp=unconfined \
  --security-opt apparmor=unconfined \
  -e PHOTOPRISM_UPLOAD_NSFW="true" \
  -e PHOTOPRISM_ADMIN_PASSWORD="insecure" \
  -v /data/docker/photoprism:/photoprism/storage \
  -v /data/media/Photos:/photoprism/originals \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  photoprism/photoprism

#DVR
#------------------------------------------
#qbittorrent
docker run -d \
  --name=qbittorrent \
  -p 6881:6881 \
  -p 6881:6881/udp \
  -v /data/docker/qbittorrent:/config \
  -v /data/downloads:/downloads \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/qbittorrent:latest

#torrent-proxy
#TBD

#Prowlarr
docker run -d \
  --name=prowlarr \
  -v /data/docker/prowlarr:/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/prowlarr:develop

#Radarr
docker run -d \
  --name=radarr \
  -v /data/docker/radarr:/config \
  -v /data/media/Movies:/movies `#optional` \
  -v /data/downloads:/downloads `#optional` \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/radarr:latest

#Sonarr
docker run -d \
  --name=sonarr \
  -v /data/docker/radarr:/config \
  -v /data/media/TV:/tv `#optional` \
  -v /data/downloads:/downloads `#optional` \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/sonarr:latest

#Readarr
docker run -d \
  --name=readarr \
  -v /data/docker/readarr:/config \
  -v /data/media/Books:/books `#optional` \
  -v /data/downloads:/downloads `#optional` \
    -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
    lscr.io/linuxserver/readarr:develop

#Lidarr
docker run -d \
  --name=lidarr \
  -v /data/docker/lidarr:/config \
  -v /data/media/Music:/music `#optional` \
  -v /data/downloads:/downloads `#optional` \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/lidarr:latest

#==========================================
#Other
#==========================================
#Syncthing
docker run -d \
  --name=syncthing \
  --hostname=syncthing `#optional` \
  -p 22000:22000/tcp \
  -p 22000:22000/udp \
  -p 21027:21027/udp \
  -v /data/docker/syncthing/config:/config \
  -v /data/syncthing:/syncthing-data \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/syncthing:latest

#FileBrowser
docker run \
  -v /data/public:/srv \
  -v /data/docker/filebrowser/database:/database\
  -v /data/docker/filebrowser/config:/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  filebrowser/filebrowser:s6
    
#Heimdall
docker run -d \
  --name=heimdall \
  -v /data/docker/heimdall:/config \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="America/Toronto" \
  --restart=unless-stopped \
  lscr.io/linuxserver/heimdall:latest
